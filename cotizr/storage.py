from flask_pymongo import PyMongo
from pymongo import DESCENDING


MONGO = PyMongo()


def save_cotizacion(entidad, cotizacion):
    MONGO.db[f'contizacion-{entidad}'].insert_one(cotizacion)


def insert_ejecucion(ejecucion):
    MONGO.db.ejecuciones.insert_one(ejecucion)


def update_ejecucion(ejecucion):
    MONGO.db.ejecuciones.find_one_and_update(
        {"id": ejecucion['id']}, {'$set': ejecucion})


def retrieve_cotizaciones():
    result = {}
    names = MONGO.db.list_collection_names()
    for name in (n for n in names if n.startswith('contizacion-')):
        cotizacion = retrieve_last_cotizacion(MONGO.db[name])
        result[cotizacion['entidad']] = cotizacion
    return result


def retrieve_last_cotizacion(collection):
    return cleanup(collection.find_one(sort=[('_id', DESCENDING)]))


def retrieve_ejecucion(ejecucion_id):
    return cleanup(MONGO.db.ejecuciones.find_one({"id": ejecucion_id}))


def cleanup(doc):
    del doc['_id']
    return doc
