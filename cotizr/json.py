from datetime import date
from flask.json import JSONEncoder
from bson import ObjectId


class CustomJSONEncoder(JSONEncoder):
    def default(self, o):  # pylint: disable=E0202
        if isinstance(o, date):
            return o.isoformat()
        if isinstance(o, ObjectId):
            return str(o)
        return JSONEncoder.default(self, o)
