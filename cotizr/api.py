from threading import Thread
from flask import Blueprint, jsonify, url_for, abort, current_app
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from cotizr import ejecuciones, storage

LIMITER = Limiter(key_func=get_remote_address)
BP = Blueprint('api', __name__, url_prefix='/api')


@BP.route('/cotizaciones')
@LIMITER.limit("200/day;30/hour;10/minute")
def get_cotizaciones():
    return jsonify(storage.retrieve_cotizaciones())


@BP.route('/ejecuciones', methods=['POST'])
@LIMITER.limit("16/day;2/hour;1/minute")
def post_ejecucion():
    ejecucion = ejecuciones.create()

    Thread(target=ejecuciones.run_ejecucion,
           args=(ejecucion, current_app.logger)).start()

    response = jsonify('ACCEPTED')
    response.status_code = 202
    response.headers['location'] = url_for(
        'api.get_ejecucion', ejecucion_id=ejecucion['id'])
    return response


@BP.route('/ejecuciones/<ejecucion_id>')
@LIMITER.limit("100/day;20/hour;5/minute")
def get_ejecucion(ejecucion_id):
    ejecucion = storage.retrieve_ejecucion(ejecucion_id)
    if ejecucion is None:
        abort(404)
    response = jsonify(ejecucion)
    response.status_code = 200
    return response
