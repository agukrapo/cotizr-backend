import re
from functools import wraps
from datetime import date, datetime
from urllib.request import urlopen
from urllib.parse import urljoin
from collections import namedtuple
from contextlib import contextmanager
from pytz import timezone
from bs4 import BeautifulSoup, SoupStrainer
from chardet import detect as detect_charset
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

REDIRECT_RE = re.compile('<meta[^>]*?url=(.*?)["\']', re.IGNORECASE)

Result = namedtuple(
    'Result', ['entidad', 'fecha', 'actualizado', 'compra', 'venta', 'url'])


def cleanup_value(value):
    return float(value.replace('$', '').replace(',', '.').strip())


@contextmanager
def selenium_browser(url):
    options = Options()
    options.headless = True
    driver = webdriver.Firefox(options=options)

    while True:
        driver.get(url)
        match = REDIRECT_RE.search(driver.page_source)
        if not match:
            break
        url = urljoin(url, match.groups()[0].strip())

    yield driver
    driver.quit()


def read_page_contents_with_urlopen(url):
    contents = decode(urlopen(url).read())
    match = REDIRECT_RE.search(contents)
    if match:
        new_url = urljoin(url, match.groups()[0].strip())
        return read_page_contents_with_urlopen(new_url)
    return contents


def read_page_contents_with_selenium(url):
    with selenium_browser(url) as browser:
        return browser.page_source


def decode(contents):
    if isinstance(contents, bytes):
        charset = detect_charset(contents)
        encoding = charset['encoding']
        contents = contents.decode(encoding)
    return contents


def extract_nacion():
    url = 'http://www.bna.com.ar/Personas'
    table_cotizacion = SoupStrainer('table', {'class': 'table cotizacion'})
    soup = BeautifulSoup(read_page_contents_with_urlopen(url), "html.parser",
                         parse_only=table_cotizacion)

    date_as_string = soup.select('th.fechaCot')[0].string
    target_date = datetime.strptime(date_as_string, '%d/%m/%Y').date()

    buy = None
    sell = None
    for row in soup.select('tbody tr'):
        tit = row.select('td[class=tit]')[0].string
        if tit == 'Dolar U.S.A':
            buy, sell = [cleanup_value(i.get_text())
                         for i in row.select('td[class=tit] ~ td')]
            break

    return Result(entidad='Nación',
                  fecha=str(target_date),
                  compra=buy,
                  venta=sell,
                  actualizado=current_timestamp(),
                  url=url)


def extract_bbva():
    url = 'https://hb.bbv.com.ar/fnet/mod/inversiones/NL-dolareuro.jsp'
    table_cotizacion = SoupStrainer('table')
    soup = BeautifulSoup(read_page_contents_with_urlopen(url), "html.parser",
                         parse_only=table_cotizacion)

    detalles = soup.select('span.detalles')
    buy = detalles[0].get_text()
    sell = detalles[1].get_text()
    return Result(entidad='BBVA',
                  fecha=str(date.today()),
                  compra=cleanup_value(buy),
                  venta=cleanup_value(sell),
                  actualizado=current_timestamp(),
                  url=url)


def extract_galicia():
    url = 'https://www.bancogalicia.com/banca/online/web/Personas/ProductosyServicios/Cotizador'
    container = SoupStrainer('li', {'id': 'Badge_0'})
    soup = BeautifulSoup(read_page_contents_with_selenium(url),
                         "html.parser", parse_only=container)

    divs = soup.select('div')
    buy = cleanup_value(divs[1].get_text())
    sell = cleanup_value(divs[2].get_text())

    return Result(entidad='Galicia',
                  fecha=str(date.today()),
                  compra=buy,
                  venta=sell,
                  actualizado=current_timestamp(),
                  url=url)


def extract_santander():
    url = 'https://banco.santanderrio.com.ar/exec/cotizacion/index.jsp'

    table = SoupStrainer('table')
    soup = BeautifulSoup(read_page_contents_with_urlopen(url), "html.parser",
                         parse_only=table)
    buy = None
    sell = None

    for row in soup.find_all('tr'):
        tds = row.find_all('td')
        if tds and tds[0].string == 'Dólar':
            buy = cleanup_value(tds[1].get_text())
            sell = cleanup_value(tds[2].get_text())

    return Result(entidad='Santander',
                  fecha=str(date.today()),
                  compra=buy,
                  venta=sell,
                  actualizado=current_timestamp(),
                  url=url)


def extract_icbc():
    url = 'https://www.icbc.com.ar/personas'

    container = SoupStrainer(
        'div', {'class': 'c-cotizacion c-cotizacion--shadow-top'})
    soup = BeautifulSoup(read_page_contents_with_selenium(url),
                         "html.parser", parse_only=container)
    valor_compra = soup.select_one('#valor_compra').get_text()
    valor_venta = soup.select_one('#valor_venta').get_text()
    valor_fechahora = soup.select_one('#valor_fechahora').get_text()

    buy = cleanup_value(valor_venta[valor_venta.index('$'):])
    sell = cleanup_value(valor_compra[valor_compra.index('$'):])
    target_date = datetime.strptime(
        valor_fechahora.split()[2], '%d/%m/%Y').date()

    return Result(entidad='ICBC',
                  fecha=str(target_date),
                  compra=buy,
                  venta=sell,
                  actualizado=current_timestamp(),
                  url=url)


def extract_bancor():
    url = 'https://www.bancor.com.ar'
    soup = BeautifulSoup(read_page_contents_with_selenium(url), "html.parser")

    buy = cleanup_value(soup.select_one('#usd-purchase').get_text())
    sell = cleanup_value(soup.select_one('#usd-sale').get_text())

    return Result(entidad='Bancor',
                  fecha=str(date.today()),
                  compra=buy,
                  venta=sell,
                  actualizado=current_timestamp(),
                  url=url)


def current_timestamp():
    return datetime.now(tz=timezone('America/Argentina/Buenos_Aires')).isoformat()


ENABLED_EXTRACTORS = {'Nación': extract_nacion,
                      'BBVA': extract_bbva,
                      'Galicia': extract_galicia,
                      'Santander': extract_santander,
                      'ICBC': extract_icbc,
                      'Bancor': extract_bancor}
