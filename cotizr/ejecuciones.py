from datetime import datetime
from pytz import timezone
from uuid import uuid4
from cotizr.extractors import ENABLED_EXTRACTORS
from cotizr.storage import save_cotizacion, insert_ejecucion, update_ejecucion


def create():
    ejecucion = {'id': str(uuid4()),
                 'status': 'PENDING',
                 'creada': current_timestamp()}
    insert_ejecucion(ejecucion)
    return ejecucion


def run_ejecucion(ejecucion, logger):
    logger.info('%s ejecucion started', ejecucion['id'])
    status = 'OK'
    detalle = {}
    for entidad, extractor in ENABLED_EXTRACTORS.items():
        try:
            extractor_status = 'ERROR'
            result = extractor()
            save_cotizacion(entidad, result._asdict())
            extractor_status = 'OK'
            logger.info('%s ejecucion, %s OK',
                        ejecucion['id'], extractor.__name__)
        except Exception:
            status = 'ERROR'
            logger.exception('%s ejecucion, %s ERROR',
                             ejecucion['id'], extractor.__name__)
        finally:
            detalle[entidad] = extractor_status
    ejecucion['detalle'] = detalle
    ejecucion['status'] = status
    ejecucion['actualizada'] = current_timestamp()
    update_ejecucion(ejecucion)
    logger.info('%s ejecucion finished, status %s', ejecucion['id'], status)


def current_timestamp():
    return datetime.now(tz=timezone('America/Argentina/Buenos_Aires')).isoformat()
