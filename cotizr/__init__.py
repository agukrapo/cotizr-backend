import os
from werkzeug.exceptions import HTTPException
from flask import Flask, send_from_directory, jsonify, g
from flask_cors import CORS
from pymongo import MongoClient, DESCENDING
from pymongo.errors import PyMongoError
from cotizr import api, storage
from cotizr.json import CustomJSONEncoder


def create_app(test_config=None):
    app = Flask(__name__)
    CORS(app)

    if test_config:
        app.config.from_mapping(test_config)
    else:
        storage.MONGO.init_app(app, os.getenv('MONGO_URI'))

    api.LIMITER.init_app(app)
    api.LIMITER.enabled = not app.debug

    app.json_encoder = CustomJSONEncoder

    app.register_blueprint(api.BP)

    @app.route('/favicon.ico')
    def favicon():
        return send_from_directory(os.path.join(app.root_path, 'static'),
                                   'favicon.ico', mimetype='image/vnd.microsoft.icon')

    @app.errorhandler(HTTPException)
    def handle_httpexception(exception):
        response = jsonify(error=str(exception))
        response.status_code = exception.code
        return response

    @app.errorhandler(Exception)
    def handle_exception(exception):
        app.logger.exception('Internal server error')
        msg = str(exception) if app.debug else 'Informacion no disponible'
        response = jsonify(error=msg)
        response.status_code = 500
        return response

    @app.route('/')
    def index():
        return f'cotizr is up in {"DEV" if app.debug else "PROD"} mode'

    return app
