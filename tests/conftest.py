import pytest
from cotizr import create_app


@pytest.fixture
def test_app():
    app = create_app({'TESTING': True})
    app.debug = False
    yield app


@pytest.fixture
def client(test_app):
    return test_app.test_client()


@pytest.fixture
def runner(test_app):
    return test_app.test_cli_runner()
