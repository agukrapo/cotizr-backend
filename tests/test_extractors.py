from datetime import date, datetime
from utils import read_page_contents
from cotizr.extractors import ENABLED_EXTRACTORS, extract_nacion, extract_santander, extract_galicia, extract_icbc, extract_bbva, extract_bancor


def test_enabled_extractors():
    assert len(ENABLED_EXTRACTORS) == 6

    for entidad, extractor in ENABLED_EXTRACTORS.items():
        assert entidad
        assert callable(extractor)


def test_nacion(mocker):
    mock_date_time = mocker.patch('cotizr.extractors.datetime')
    mock_date_time.now.return_value = datetime(2015, 12, 31, 23, 59, 30)
    mock_date_time.strptime = datetime.strptime
    mock_contents = mocker.patch(
        'cotizr.extractors.read_page_contents_with_urlopen')
    mock_contents.return_value = read_page_contents('nacion')

    entidad, fecha, actualizado, compra, venta, url = extract_nacion()
    assert entidad == 'Nación'
    assert fecha == '2019-07-05'
    assert actualizado == '2015-12-31T23:59:30'
    assert compra == 41.1
    assert venta == 43.1
    assert url == 'http://www.bna.com.ar/Personas'


def test_santander(mocker):
    mock_date = mocker.patch('cotizr.extractors.date')
    mock_date.today.return_value = date(2010, 2, 25)
    mock_date_time = mocker.patch('cotizr.extractors.datetime')
    mock_date_time.now.return_value = datetime(2015, 12, 31, 23, 59, 30)
    mock_contents = mocker.patch(
        'cotizr.extractors.read_page_contents_with_urlopen')
    mock_contents.return_value = read_page_contents('santander')

    entidad, fecha, actualizado, compra, venta, url = extract_santander()
    assert entidad == 'Santander'
    assert fecha == '2010-02-25'
    assert actualizado == '2015-12-31T23:59:30'
    assert compra == 40.68
    assert venta == 43.43
    assert url == 'https://banco.santanderrio.com.ar/exec/cotizacion/index.jsp'


def test_galicia(mocker):
    mock_date = mocker.patch('cotizr.extractors.date')
    mock_date.today.return_value = date(2010, 2, 25)
    mock_date_time = mocker.patch('cotizr.extractors.datetime')
    mock_date_time.now.return_value = datetime(2015, 12, 31, 23, 59, 30)
    mock_contents = mocker.patch(
        'cotizr.extractors.read_page_contents_with_selenium')
    mock_contents.return_value = read_page_contents('galicia')

    entidad, fecha, actualizado, compra, venta, url = extract_galicia()
    assert entidad == 'Galicia'
    assert fecha == '2010-02-25'
    assert actualizado == '2015-12-31T23:59:30'
    assert compra == 40.9
    assert venta == 43.4
    assert url == 'https://www.bancogalicia.com/banca/online/web/' + \
        'Personas/ProductosyServicios/Cotizador'


def test_icbc(mocker):
    mock_date_time = mocker.patch('cotizr.extractors.datetime')
    mock_date_time.now.return_value = datetime(2015, 12, 31, 23, 59, 30)
    mock_date_time.strptime = datetime.strptime
    mock_contents = mocker.patch(
        'cotizr.extractors.read_page_contents_with_selenium')
    mock_contents.return_value = read_page_contents('icbc')

    entidad, fecha, actualizado, compra, venta, url = extract_icbc()
    assert entidad == 'ICBC'
    assert fecha == '2019-07-17'
    assert actualizado == '2015-12-31T23:59:30'
    assert compra == 41.6
    assert venta == 44
    assert url == 'https://www.icbc.com.ar/personas'


def test_bbva(mocker):
    mock_date = mocker.patch('cotizr.extractors.date')
    mock_date.today.return_value = date(2010, 2, 25)
    mock_date_time = mocker.patch('cotizr.extractors.datetime')
    mock_date_time.now.return_value = datetime(2015, 12, 31, 23, 59, 30)
    mock_contents = mocker.patch(
        'cotizr.extractors.read_page_contents_with_urlopen')
    mock_contents.return_value = read_page_contents('bbva')

    entidad, fecha, actualizado, compra, venta, url = extract_bbva()
    assert entidad == 'BBVA'
    assert fecha == '2010-02-25'
    assert actualizado == '2015-12-31T23:59:30'
    assert compra == 41.37
    assert venta == 43.73
    assert url == 'https://hb.bbv.com.ar/fnet/mod/inversiones/NL-dolareuro.jsp'


def test_bancor(mocker):
    mock_date = mocker.patch('cotizr.extractors.date')
    mock_date.today.return_value = date(2010, 2, 25)
    mock_date_time = mocker.patch('cotizr.extractors.datetime')
    mock_date_time.now.return_value = datetime(2015, 12, 31, 23, 59, 30)
    mock_contents = mocker.patch(
        'cotizr.extractors.read_page_contents_with_selenium')
    mock_contents.return_value = read_page_contents('bancor')

    entidad, fecha, actualizado, compra, venta, url = extract_bancor()
    assert entidad == 'Bancor'
    assert fecha == '2010-02-25'
    assert actualizado == '2015-12-31T23:59:30'
    assert compra == 55.25
    assert venta == 59.5
    assert url == 'https://www.bancor.com.ar'
