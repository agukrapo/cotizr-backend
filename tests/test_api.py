from flask import json


def test_get_cotizaciones(client, mocker):
    results = {'some_bank': {'entidad': 'some_bank',
                             'fecha': 'today',
                             'compra': 'buy',
                             'venta': 'sell',
                             'actualizado': 'now'}}
    mock = mocker.patch('cotizr.api.storage.retrieve_cotizaciones')
    mock.return_value = results

    response = client.get('/api/cotizaciones')

    assert json.loads(response.data) == results
    assert response.status == '200 OK'


def test_post_ejecucion(client, mocker):
    mocker.patch('cotizr.api.ejecuciones.run_ejecucion')
    mocker.patch('cotizr.api.ejecuciones.create').return_value = {'id': '123'}

    response = client.post('/api/ejecuciones')

    assert json.loads(response.data) == 'ACCEPTED'
    assert response.status == '202 ACCEPTED'
    assert response.headers['Location'] == 'http://localhost/api/ejecuciones/123'


def test_get_ejecucion(client, mocker):
    ejecucion = {'id': '456', 'status': 'OK'}
    mock = mocker.patch('cotizr.api.storage.retrieve_ejecucion')
    mock.return_value = ejecucion

    response = client.get('/api/ejecuciones/456')

    assert json.loads(response.data) == ejecucion
    assert response.status == '200 OK'
