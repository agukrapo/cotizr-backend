from cotizr import create_app


def test_config():
    assert create_app({'TESTING': True}).testing


def test_index(client):
    response = client.get('/')

    assert response.data == b'cotizr is up in PROD mode'
    assert response.status == '200 OK'
