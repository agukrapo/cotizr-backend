
from pathlib import Path
from cotizr.extractors import read_page_contents_with_selenium


def read_page_contents(extractor_name):
    parent = Path(__file__).parent
    path = parent / 'data' / f'{extractor_name}_page_contents.txt'
    return path.absolute().read_bytes().decode('utf-8')


def write_page_contents(name, contents):
    parent = Path(__file__).parent
    path = parent / 'data' / f'{name}_page_contents.txt'
    path.write_bytes(contents.encode('utf-8'))
    print(f'Writing contents to {path}')


if __name__ == '__main__':
    write_page_contents('bancor', read_page_contents_with_selenium(
        'https://www.bancor.com.ar'))
