cotizr-backend
==============

Live demo: https://powerful-tor-41992.herokuapp.com

Virtual environment
-------------------
::

    python3 -m venv venv && \
    . venv/bin/activate && \
    python3 -m pip install --upgrade pip

Install app
-----------
::

    pip3 install -e .

Package and Install app
-----------------------
::

    pip3 install wheel && \
    python setup.py bdist_wheel && \
    pip3 install dist/*.whl

Run tests
---------
::

    pytest

Run app in development
----------------------
::

    export FLASK_ENV=development && \
    export FLASK_APP=cotizr && \
    export MONGO_URI={...} && \
    flask run --port=4321

Run app with Docker
-------------------
::

    python setup.py bdist_wheel && \
    sudo docker build -t cotizr:latest . && \
    sudo docker run -p 4321:4321 -e "PORT=4321" -e "MONGO_URI={...}" cotizr

Selemium & geckodriver
----------------------
Some extractors use selenium in order to parse javascript.
Selenium is configured to use geckodriver.
Please download and place it in PATH.
https://github.com/mozilla/geckodriver/releases

Deploy in Heroku
----------------
::

    heroku container:login
    heroku create
    heroku config:set MONGO_URI={...}
    heroku container:push web -a APP_NAME
    heroku container:release web -a APP_NAME
