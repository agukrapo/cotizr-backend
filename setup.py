from setuptools import find_packages, setup

setup(
    name='cotizr',
    version='1.0.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask', 'flask-limiter', 'flask-cors',
        'beautifulsoup4',
        'selenium',
        'pytest', 'pytest-mock',
        'pytz',
        'chardet',
        'Flask-PyMongo', 'pymongo[srv]',
    ],
)
