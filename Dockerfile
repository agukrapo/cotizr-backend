FROM python:3.6.9-alpine3.10

# Intall firefox & geckodriver
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
RUN wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.30-r0/glibc-2.30-r0.apk
RUN wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.30-r0/glibc-bin-2.30-r0.apk
RUN apk add glibc-2.30-r0.apk
RUN apk add glibc-bin-2.30-r0.apk
RUN apk add firefox-esr=60.9.0-r0
RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-linux64.tar.gz
RUN tar -zxf geckodriver-v0.26.0-linux64.tar.gz -C /usr/bin
RUN geckodriver --version

# Install & run app
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN mkdir -p /usr/local/src/cotizr
WORKDIR /usr/local/src/cotizr
COPY ./dist/*.whl ./
RUN pip3 install *.whl
RUN pip3 install gunicorn
CMD gunicorn --bind 0.0.0.0:$PORT cotizr.wsgi:APP
